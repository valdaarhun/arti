//! Provides [`StreamPollSet`]

// So that we can declare these things as if they were in their own crate.
#![allow(unreachable_pub)]

use std::{
    collections::{btree_map, hash_map, BTreeMap, HashMap},
    hash::Hash,
    task::Poll,
};

use futures::stream::StreamExt;

use crate::util::keyed_futures_unordered::KeyedFuturesUnordered;

/// Represents a potentially-buffered value read from a stream. Returned by
/// [`StreamPollSet::remove`].
#[derive(Debug, Eq, PartialEq)]
pub enum BufferedPollNextResult<T> {
    /// No buffered result.
    None,
    /// Received end-of-stream (`None`).
    StreamEnd,
    /// A value read from the stream.
    Value(T),
}

/// Manages a dynamic set of [`futures::Stream`] with associated keys and
/// priorities.
///
/// Notable features:
///
/// * Prioritization: streams have an associated priority, and ready-streams are
///   iterated over in ascending priority order.
/// * Streams are effectively "peekable". Internally reads and buffers at most
///   one item from each stream. These can be inspected prior to extracting the
///   item. e.g. calling code can use this to determine whether it's actually
///   ready to process a particular item before extracting it; if not it can be
///   left in place (providing back-pressure to a corresponding `Sink` when
///   applicable) and the next ready items from other streams can still be
///   serviced.
/// * Efficient polling: an unready stream won't be polled again until it's
///   ready or exhausted (e.g. a corresponding [`futures::Sink`] is written-to or
///   dropped). A ready stream won't be polled again until the ready item has been
///   removed.
pub struct StreamPollSet<K, P, S>
where
    S: futures::Stream + Unpin,
{
    /// Priority for each stream in the set.
    // We keep the priority for each stream here instead of bundling it together
    // with the stream, so that the priority can easily be changed even while a
    // future waiting on the stream is still pending (e.g. to support rescaling
    // priorities for EWMA).
    // Invariants:
    // * Every key is also present in exactly one of `ready_values` or `pending_streams`.
    priorities: HashMap<K, P>,
    /// Streams that have a result ready, in ascending order by priority.
    // Invariants:
    // * Keys are a (non-strict) subset of those in `priorities`.
    #[allow(clippy::type_complexity)]
    ready_values: BTreeMap<(P, K), (Option<S::Item>, S)>,
    /// Streams for which we're still waiting for the next result.
    // Invariants:
    // * Keys are a (non-strict) subset of those in `priorities`.
    pending_streams: KeyedFuturesUnordered<K, futures::stream::StreamFuture<S>>,
}

impl<K, P, S> StreamPollSet<K, P, S>
where
    K: Ord + Hash + Clone + Send + Sync + 'static,
    S: futures::Stream + Unpin,
    P: Ord + Clone,
{
    /// Create a new, empty, `StreamPollSet`.
    pub fn new() -> Self {
        Self {
            priorities: Default::default(),
            ready_values: Default::default(),
            pending_streams: KeyedFuturesUnordered::new(),
        }
    }

    /// Insert a `stream`, with an associated `key` and `priority`.
    ///
    /// If the `key` is already in use, the parameters are returned without altering `self`.
    // To *replace* an existing key, we'd need to cancel any pending future and
    // ensure that the cancelation is processed before inserting the new key, to
    // ensure we don't assign a value from the previous key to the new key's
    // stream.
    pub fn try_insert(
        &mut self,
        key: K,
        priority: P,
        stream: S,
    ) -> Result<(), KeyAlreadyInsertedError<K, P, S>> {
        let hash_map::Entry::Vacant(v) = self.priorities.entry(key.clone()) else {
            // We already have an entry for this key.
            return Err(KeyAlreadyInsertedError {
                key,
                priority,
                stream,
            });
        };
        self.pending_streams
            .try_insert(key, stream.into_future())
            // By `pending_streams` invariant that keys are a subset of those in
            // `priorities`.
            .unwrap_or_else(|_| panic!("Unexpected duplicate key"));
        v.insert(priority);
        Ok(())
    }

    /// Remove the entry for `key`, if any. This is the key, priority, buffered
    /// poll_next result, and stream.
    pub fn remove(&mut self, key: &K) -> Option<(K, P, BufferedPollNextResult<S::Item>, S)> {
        let priority = self.priorities.remove(key)?;
        if let Some((key, stream_fut)) = self.pending_streams.remove(key) {
            // Validate `priorities` invariant that keys are also present in exactly one of
            // `pending_streams` and `ready_values`.
            debug_assert!(!self
                .ready_values
                .contains_key(&(priority.clone(), key.clone())));
            let stream = stream_fut
                .into_inner()
                // We know the future hasn't completed, so the stream should be present.
                .expect("Missing stream");
            Some((key, priority, BufferedPollNextResult::None, stream))
        } else {
            let ((_priority, key), (value, stream)) = self
                .ready_values
                .remove_entry(&(priority.clone(), key.clone()))
                // By
                // * `pending_streams` invariant that keys are also present in
                // exactly one of `pending_streams` and `ready_values`.
                // * validated above that the key was in `pending_streams`, and
                // not in `ready_values`.
                .expect("Unexpectedly no value for key");
            let buf = match value {
                Some(x) => BufferedPollNextResult::Value(x),
                None => BufferedPollNextResult::StreamEnd,
            };
            Some((key, priority, buf, stream))
        }
    }

    /// Polls streams that are ready to be polled, and returns an iterator over all streams
    /// for which we have a buffered `Poll::Ready` result, in ascending priority order.
    ///
    /// Registers the provided [`Context`][std::task::Context] to be woken when
    /// any of the internal streams that weren't ready in the previous call to
    /// this method (and therefore wouldn't have appeared in the iterator
    /// results) become potentially ready (based on when the inner stream wakes
    /// the `Context` provided to its own `poll_next`).
    ///
    /// The iterator values include the key, priority, and the buffered
    /// [`Poll::Ready`] result from calling [`futures::Stream::poll_next`]. i.e.
    /// either `Some` value read from the stream, or a `None` indicating that
    /// the `Stream` is exhausted.
    ///
    /// The same restrictions apply as for [`Self::stream_mut`] with respect to
    /// accessing the Stream object (e.g. using interior mutability).
    ///
    /// This method does *not* drain ready items. `Some` values can be removed
    /// with [`Self::take_ready_value_and_reprioritize`]. `None` values can only
    /// be removed by removing the whole stream with [`Self::remove`].
    ///
    /// This API is meant to allow callers to find the first stream (in priority
    /// order) that is ready, and that the caller is able to process now. i.e.
    /// it's specifically to support the use-case where external factors may
    /// prevent the processing of some streams but not others.
    ///
    /// Example:
    ///
    /// ```nocompile
    /// # // We need the `nocompile` since `StreamPollSet` is non-pub.
    /// # // TODO: take away the nocompile if we make this pub or implement some
    /// # // workaround to expose it to doc-tests.
    /// # type Key=u64;
    /// # type Value=u64;
    /// # type Priority=u64;
    /// # type MyStream=Box<dyn futures::Stream<Item=Value> + Unpin>;
    /// # fn can_process(key: &Key, val: &Value) -> bool { true }
    /// # fn process(val: Value) { }
    /// # fn new_priority(priority: &Priority) -> Priority { *priority }
    /// fn process_a_ready_stream(sps: &mut StreamPollSet<Key, Value, Priority, MyStream>, cx: &mut std::task::Context) -> std::task::Poll<()> {
    ///   let mut iter = sps.poll_ready_iter(cx);
    ///   while let Some((key, value, priority, _stream)) = iter.next() {
    ///     let Some(value) = value else {
    ///        // Stream exhausted. Remove the stream. We have to drop the iterator
    ///        // first, though, so that we can mutate.
    ///        let key = *key;
    ///        drop(iter);
    ///        sps.remove(&key).unwrap();
    ///        return std::task::Poll::Ready(());
    ///     };
    ///     if can_process(key, value) {
    ///        let key = *key;
    ///        let priority = new_priority(priority);
    ///        drop(iter);
    ///        let (_old_priority, value) = sps.take_ready_value_and_reprioritize(&key, priority).unwrap();
    ///        process(value);
    ///        return std::task::Poll::Ready(());
    ///     }
    ///   }
    ///   return std::task::Poll::Pending;
    /// }
    /// ```
    // TODO: Alternatively we could perhaps take some sort of "Future factory"
    // when inserting a stream that, given the next item in the stream, returns
    // a future that completes when that item can actually be processed. Or
    // maybe processes the item. That seems like a fair bit of generics and
    // ownership complexity though; deferring for the moment.
    //
    // TODO: It would be nice if the returned iterator supported additional
    // actions, e.g. allowing the user to consume the iterator and take and
    // reprioritize the inner value. I *think* we'd either need to make a
    // self-referential type holding both a reference and the inner iterator, or
    // else keep a copy of the current position `(K, P)` and do O(log(N))
    // lookups on each access, though.
    pub fn poll_ready_iter<'a>(
        &'a mut self,
        cx: &mut std::task::Context,
    ) -> impl Iterator<Item = (&'a K, Option<&'a S::Item>, &'a P, &'a S)> + 'a {
        // First poll for ready streams
        while let Poll::Ready(Some((key, (value, stream)))) =
            self.pending_streams.poll_next_unpin(cx)
        {
            let priority = self
                .priorities
                .get(&key)
                // By `pending_streams` invariant that all keys are also in `priorities`.
                .expect("Missing priority");
            let prev = self
                .ready_values
                .insert((priority.clone(), key), (value, stream));
            assert!(prev.is_none());
        }
        self.ready_values
            .iter()
            .map(|((p, k), (v, s))| (k, v.as_ref(), p, s))
    }

    /// If the stream for `key` has `Some(value)` ready, take that value and set the
    /// priority for it to `new_priority`.
    ///
    /// This method doesn't poll the internal streams. Use `poll_ready_iter` to
    /// ensure streams make progress.
    ///
    /// If the key doesn't exist, the stream isn't ready, or the stream's value
    /// is `None` (indicating the end of the stream), this function returns
    /// `None` without mutating anything.
    ///
    /// Ended streams should be removed using [`Self::remove`].
    pub fn take_ready_value_and_reprioritize(
        &mut self,
        key: &K,
        new_priority: P,
    ) -> Option<(P, S::Item)> {
        // Get the priority entry, but don't replace until the lookup in ready_streams is confirmed.
        let hash_map::Entry::Occupied(mut priority_entry) = self.priorities.entry(key.clone())
        else {
            // Key isn't present at all.
            return None;
        };
        let priority_mut = priority_entry.get_mut();
        let btree_map::Entry::Occupied(ready_stream_entry) =
            self.ready_values.entry((priority_mut.clone(), key.clone()))
        else {
            // This stream isn't ready.
            return None;
        };
        #[allow(clippy::question_mark)]
        if ready_stream_entry.get().0.is_none() {
            // The stream is ready, but it's at end-of-stream. It doesn't have a value.
            return None;
        }
        let prev_priority = std::mem::replace(priority_mut, new_priority);
        let ((_p, key), (value, stream)) = ready_stream_entry.remove_entry();
        let value = value
            // Checked above.
            .expect("Value disappeared");
        self.pending_streams
            .try_insert(key, stream.into_future())
            // We verified above that the key wasn't present in `priorities`,
            // and `pending_streams` has the invariant that its keys are a
            // subset of those in `priorities`.
            .unwrap_or_else(|_| panic!("Unexpected pending stream entry"));
        Some((prev_priority, value))
    }

    /// Get a mut reference to a ready value for key `key`, if one exists.
    ///
    /// This method doesn't poll the internal streams. Use `poll_ready_iter` to
    /// ensure streams make progress.
    // This will be used for packing and fragmentation, to take part of a DATA message.
    #[allow(unused)]
    pub fn ready_value_mut(&mut self, key: &K) -> Option<&mut S::Item> {
        let priority = self.priorities.get(key)?;
        let value = &mut self
            .ready_values
            .get_mut(&(priority.clone(), key.clone()))?
            .0;
        value.as_mut()
    }

    /// Get a reference to the stream for `key`.
    ///
    /// The same restrictions apply as for [`Self::stream_mut`] (e.g. using
    /// interior mutability).
    #[allow(dead_code)]
    pub fn stream(&self, key: &K) -> Option<&S> {
        if let Some(s) = self.pending_streams.get(key) {
            let s = s.get_ref();
            // Stream must be present since it's still pending.
            debug_assert!(s.is_some(), "Unexpected missing pending stream");
            return s;
        }
        let priority = self.priorities.get(key)?;
        self.ready_values
            .get(&(priority.clone(), key.clone()))
            .map(|(_v, s)| s)
    }

    /// Get a mut reference to the stream for `key`.
    ///
    /// Polling the stream through this reference, or otherwise causing its
    /// registered `Waker` to be removed without waking it, will result in
    /// unspecified (but not unsound) behavior.
    ///
    /// This is mostly intended for accessing non-`Stream` functionality of the stream
    /// object, though it *is* permitted to mutate it in a way that the stream becomes
    /// ready (potentially removing and waking its registered Waker(s)).
    //
    // In particular:
    // * Doing so and getting a Pending result will override the internal waker
    //   used in KeyedFuturesUnordered, causing it not to be notified when the
    //   stream is ready.
    // * Pulling items out of the stream directly may "jump the line" in front
    //   of a ready item we've already pulled from the stream.
    pub fn stream_mut(&mut self, key: &K) -> Option<&mut S> {
        if let Some(s) = self.pending_streams.get_mut(key) {
            let s = s.get_mut();
            // Stream must be present since it's still pending.
            debug_assert!(s.is_some(), "Unexpected missing pending stream");
            return s;
        }
        let priority = self.priorities.get(key)?;
        self.ready_values
            .get_mut(&(priority.clone(), key.clone()))
            .map(|(_v, s)| s)
    }

    /// Number of streams managed by this object.
    pub fn len(&self) -> usize {
        self.priorities.len()
    }
}

/// Error returned by [`StreamPollSet::try_insert`].
#[derive(Debug, thiserror::Error)]
#[allow(clippy::exhaustive_structs)]
pub struct KeyAlreadyInsertedError<K, P, S> {
    /// Key that caller tried to insert.
    #[allow(dead_code)]
    pub key: K,
    /// Priority that caller tried to insert.
    #[allow(dead_code)]
    pub priority: P,
    /// Stream that caller tried to insert.
    #[allow(dead_code)]
    pub stream: S,
}

#[cfg(test)]
mod test {
    // @@ begin test lint list maintained by maint/add_warning @@
    #![allow(clippy::bool_assert_comparison)]
    #![allow(clippy::clone_on_copy)]
    #![allow(clippy::dbg_macro)]
    #![allow(clippy::mixed_attributes_style)]
    #![allow(clippy::print_stderr)]
    #![allow(clippy::print_stdout)]
    #![allow(clippy::single_char_pattern)]
    #![allow(clippy::unwrap_used)]
    #![allow(clippy::unchecked_duration_subtraction)]
    #![allow(clippy::useless_vec)]
    #![allow(clippy::needless_pass_by_value)]
    //! <!-- @@ end test lint list maintained by maint/add_warning @@ -->

    use std::{
        collections::VecDeque,
        sync::{Arc, Mutex},
        task::Poll,
    };

    use futures::SinkExt as _;
    use tor_rtmock::MockRuntime;

    use super::*;

    #[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
    struct Key(u64);

    #[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
    struct Priority(u64);

    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    struct Value(u64);

    /// Test stream that we can directly manipulate and examine.
    #[derive(Debug)]
    struct VecDequeStream<T> {
        // Ready items.
        vec: VecDeque<T>,
        // Whether any more items will be written.
        closed: bool,
        // Registered waker.
        waker: Option<std::task::Waker>,
    }
    impl<T> VecDequeStream<T> {
        fn new_open<I: IntoIterator<Item = T>>(values: I) -> Self {
            Self {
                vec: VecDeque::from_iter(values),
                waker: None,
                closed: false,
            }
        }
        fn new_closed<I: IntoIterator<Item = T>>(values: I) -> Self {
            Self {
                vec: VecDeque::from_iter(values),
                waker: None,
                closed: true,
            }
        }
        fn push(&mut self, value: T) {
            assert!(!self.closed);
            self.vec.push_back(value);
            if let Some(waker) = self.waker.take() {
                waker.wake();
            }
        }
    }
    impl<T> futures::Stream for VecDequeStream<T>
    where
        T: Unpin,
    {
        type Item = T;

        fn poll_next(
            mut self: std::pin::Pin<&mut Self>,
            cx: &mut std::task::Context<'_>,
        ) -> Poll<Option<Self::Item>> {
            if let Some(val) = self.as_mut().vec.pop_front() {
                Poll::Ready(Some(val))
            } else if self.as_mut().closed {
                // No more items coming.
                Poll::Ready(None)
            } else {
                self.as_mut().waker.replace(cx.waker().clone());
                Poll::Pending
            }
        }
    }
    impl<T> std::cmp::PartialEq for VecDequeStream<T>
    where
        T: std::cmp::PartialEq,
    {
        fn eq(&self, other: &Self) -> bool {
            // Ignore waker, which isn't comparable
            self.vec == other.vec && self.closed == other.closed
        }
    }
    impl<T> std::cmp::Eq for VecDequeStream<T> where T: std::cmp::Eq {}

    type TestStream = VecDequeStream<Value>;

    #[test]
    fn test_empty() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, TestStream>::new();
            assert_eq!(pollset.poll_ready_iter(ctx).collect::<Vec<_>>(), vec![]);
            Poll::Ready(())
        }));
    }

    #[test]
    fn test_one_pending() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, TestStream>::new();
            pollset
                .try_insert(Key(0), Priority(0), TestStream::new_open([]))
                .unwrap();
            assert_eq!(pollset.poll_ready_iter(ctx).collect::<Vec<_>>(), vec![]);
            Poll::Ready(())
        }));
    }

    #[test]
    fn test_one_ready() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, TestStream>::new();
            pollset
                .try_insert(
                    Key(0),
                    Priority(0),
                    TestStream::new_closed([Value(1), Value(2)]),
                )
                .unwrap();

            // We only see the first value of the one ready stream.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(1)),
                    &Priority(0),
                    &TestStream::new_closed([Value(2)])
                )],
            );

            // Same result, the same value is still at the head of the stream..
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(1)),
                    &Priority(0),
                    &TestStream::new_closed([Value(2)])
                )]
            );

            // Take the head of the stream.
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(0), Priority(1)),
                Some((Priority(0), Value(1)))
            );

            // Should see the next value, with the new priority.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(2)),
                    &Priority(1),
                    &TestStream::new_closed([])
                )]
            );

            // Take again.
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(0), Priority(2)),
                Some((Priority(1), Value(2)))
            );

            // Should see end-of-stream.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(&Key(0), None, &Priority(2), &TestStream::new_closed([]))]
            );

            // Remove the now-ended stream.
            assert_eq!(
                pollset.remove(&Key(0)),
                Some((
                    Key(0),
                    Priority(2),
                    BufferedPollNextResult::StreamEnd,
                    TestStream::new_closed([])
                ))
            );

            // Should now be empty.
            assert_eq!(pollset.poll_ready_iter(ctx).collect::<Vec<_>>(), vec![]);

            Poll::Ready(())
        }));
    }

    #[test]
    fn test_round_robin() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, TestStream>::new();
            pollset
                .try_insert(
                    Key(0),
                    Priority(0),
                    TestStream::new_closed([Value(1), Value(2)]),
                )
                .unwrap();
            pollset
                .try_insert(
                    Key(1),
                    Priority(1),
                    TestStream::new_closed([Value(3), Value(4)]),
                )
                .unwrap();

            // Should see both ready streams, in priority order.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![
                    (
                        &Key(0),
                        Some(&Value(1)),
                        &Priority(0),
                        &TestStream::new_closed([Value(2)])
                    ),
                    (
                        &Key(1),
                        Some(&Value(3)),
                        &Priority(1),
                        &TestStream::new_closed([Value(4)])
                    ),
                ]
            );

            // Take from the first stream and send it to the back via priority assignment.
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(0), Priority(2)),
                Some((Priority(0), Value(1)))
            );

            // Should see both ready streams, in the new priority order.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![
                    (
                        &Key(1),
                        Some(&Value(3)),
                        &Priority(1),
                        &TestStream::new_closed([Value(4)])
                    ),
                    (
                        &Key(0),
                        Some(&Value(2)),
                        &Priority(2),
                        &TestStream::new_closed([])
                    ),
                ]
            );

            // Keep going ...
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(1), Priority(3)),
                Some((Priority(1), Value(3)))
            );
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![
                    (
                        &Key(0),
                        Some(&Value(2)),
                        &Priority(2),
                        &TestStream::new_closed([])
                    ),
                    (
                        &Key(1),
                        Some(&Value(4)),
                        &Priority(3),
                        &TestStream::new_closed([])
                    ),
                ]
            );
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(0), Priority(4)),
                Some((Priority(2), Value(2)))
            );
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![
                    (
                        &Key(1),
                        Some(&Value(4)),
                        &Priority(3),
                        &TestStream::new_closed([])
                    ),
                    (&Key(0), None, &Priority(4), &TestStream::new_closed([])),
                ]
            );
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(1), Priority(5)),
                Some((Priority(3), Value(4)))
            );
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![
                    (&Key(0), None, &Priority(4), &TestStream::new_closed([])),
                    (&Key(1), None, &Priority(5), &TestStream::new_closed([])),
                ]
            );

            Poll::Ready(())
        }));
    }

    #[test]
    fn test_remove_and_reuse_key() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, TestStream>::new();
            pollset
                .try_insert(
                    Key(0),
                    Priority(0),
                    TestStream::new_closed([Value(1), Value(2)]),
                )
                .unwrap();
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(1)),
                    &Priority(0),
                    &TestStream::new_closed([Value(2)])
                ),]
            );
            assert_eq!(
                pollset.remove(&Key(0)),
                Some((
                    Key(0),
                    Priority(0),
                    BufferedPollNextResult::Value(Value(1)),
                    TestStream::new_closed([Value(2)])
                ))
            );
            pollset
                .try_insert(
                    Key(0),
                    Priority(1),
                    TestStream::new_closed([Value(3), Value(4)]),
                )
                .unwrap();
            // Ensure we see the ready value in the new stream, and *not* anything from the previous stream at that key.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(3)),
                    &Priority(1),
                    &TestStream::new_closed([Value(4)])
                ),]
            );
            Poll::Ready(())
        }));
    }

    #[test]
    fn get_ready_stream() {
        futures::executor::block_on(futures::future::poll_fn(|_ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, VecDequeStream<Value>>::new();
            pollset
                .try_insert(Key(0), Priority(0), VecDequeStream::new_open([Value(1)]))
                .unwrap();
            assert_eq!(pollset.stream(&Key(0)).unwrap().vec[0], Value(1));
            Poll::Ready(())
        }));
    }

    #[test]
    fn get_pending_stream() {
        futures::executor::block_on(futures::future::poll_fn(|_ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, VecDequeStream<Value>>::new();
            pollset
                .try_insert(Key(0), Priority(0), VecDequeStream::new_open([]))
                .unwrap();
            assert!(pollset.stream(&Key(0)).unwrap().vec.is_empty());
            Poll::Ready(())
        }));
    }

    #[test]
    fn mutate_pending_stream() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, VecDequeStream<Value>>::new();
            pollset
                .try_insert(Key(0), Priority(0), VecDequeStream::new_open([]))
                .unwrap();
            assert_eq!(pollset.poll_ready_iter(ctx).collect::<Vec<_>>(), vec![]);

            // This should cause the stream to become ready.
            pollset.stream_mut(&Key(0)).unwrap().push(Value(0));

            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(0)),
                    &Priority(0),
                    &VecDequeStream::new_open([])
                ),]
            );

            Poll::Ready(())
        }));
    }

    #[test]
    fn mutate_ready_stream() {
        futures::executor::block_on(futures::future::poll_fn(|ctx| {
            let mut pollset = StreamPollSet::<Key, Priority, VecDequeStream<Value>>::new();
            pollset
                .try_insert(Key(0), Priority(0), VecDequeStream::new_open([Value(0)]))
                .unwrap();
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(0)),
                    &Priority(0),
                    &VecDequeStream::new_open([])
                ),]
            );

            pollset.stream_mut(&Key(0)).unwrap().push(Value(1));

            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(0)),
                    &Priority(0),
                    &VecDequeStream::new_open([Value(1)])
                ),]
            );

            // Consume the value that was there.
            assert_eq!(
                pollset.take_ready_value_and_reprioritize(&Key(0), Priority(0)),
                Some((Priority(0), Value(0)))
            );

            // We should now see the value we added.
            assert_eq!(
                pollset.poll_ready_iter(ctx).collect::<Vec<_>>(),
                vec![(
                    &Key(0),
                    Some(&Value(1)),
                    &Priority(0),
                    &VecDequeStream::new_open([])
                ),]
            );

            Poll::Ready(())
        }));
    }

    #[test]
    fn test_async() {
        MockRuntime::test_with_various(|rt| async move {
            let mut pollset =
                StreamPollSet::<Key, Priority, futures::channel::mpsc::Receiver<Value>>::new();

            // Create 2 mpsc channels, bounded so that we can exercise back-pressure.
            // These are analogous to Tor streams.
            for streami in 1..=2 {
                let (mut send, recv) = futures::channel::mpsc::channel::<Value>(2);
                pollset
                    .try_insert(Key(streami), Priority(streami), recv)
                    .unwrap();
                rt.spawn_identified(format!("stream{streami}"), async move {
                    for val in 0..10 {
                        send.send(Value(val * streami)).await.unwrap();
                    }
                });
            }

            let output = Arc::new(Mutex::new(Vec::new()));

            rt.spawn_identified("mux", {
                let output = output.clone();
                async move {
                    loop {
                        let (key, value, priority) = futures::future::poll_fn(|ctx| match pollset
                            .poll_ready_iter(ctx)
                            .next()
                        {
                            Some((key, value, priority, _stream)) => {
                                Poll::Ready((*key, value.copied(), *priority))
                            }
                            // No streams ready, but there could be more items coming.
                            // The current `ctx` should be registered to wake us
                            // if and when there are.
                            None => Poll::Pending,
                        })
                        .await;
                        if let Some(value) = value {
                            // Take the value, and haphazardly set priority to push this stream "back".
                            pollset
                                .take_ready_value_and_reprioritize(&key, Priority(priority.0 + 10))
                                .unwrap();
                            output.lock().unwrap().push((key, value));
                        } else {
                            // Stream ended. Remove it.
                            pollset.remove(&key).unwrap();
                        }
                    }
                }
            });

            rt.advance_until_stalled().await;

            let output = output.lock().unwrap();

            // We can't predict exactly how the stream values will be
            // interleaved, but we should get all items from each stream, with
            // correct order within each stream.
            for streami in 1..=2 {
                let expected = (0..10).map(|val| Value(val * streami)).collect::<Vec<_>>();
                let actual = output
                    .iter()
                    .filter_map(|(k, v)| (k == &Key(streami)).then_some(*v))
                    .collect::<Vec<_>>();
                assert_eq!(actual, expected);
            }
        });
    }
}
